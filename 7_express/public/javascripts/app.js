var node = angular.module('node', ['ngRoute', 'ui.router'])

node.config(["$stateProvider", "$urlRouterProvider",
	function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/');
	//view-k hozzáadása
	$stateProvider.state('user', {
		url: '/',
		templateUrl: "/partials/user",
		controller:   "UserCtrl",
		controllerAs: "UserCtrl",
	}).state('newUser', {
		url: '/newUser',
		templateUrl: "/partials/newUser",
		controller:   "NewUserCtrl",
		controllerAs: "NewUserCtrl",
	});
}]);
