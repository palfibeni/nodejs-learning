var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var config = require('../config');
var fileHandler = require('../module/fileHandler');
const _ = require('lodash');

/* GET all */
router.get('/:fileName', function(req, res, next) {
  let filePath = path.join(config.rootPath, 'json', req.params.fileName + '.json');
  res.sendFile(filePath);
});

/* GET by id */
router.get('/:fileName/:id', function(req, res, next) {
  let filePath = path.join(config.rootPath, 'json', req.params.fileName + '.json');
  findRecordInFile(filePath, {id: req.params.id})
    .then((data) => {
      res.json(data.obj);
    }, (e) => {
      var err = new Error('Json File not Found');
      err.status = 404;
      next(err);
    });
});

/* INSERT */
router.post('/:fileName/new', function(req, res, next) {
    let filePath = path.join(config.rootPath, 'json', req.params.fileName + '.json');
    fileHandler.readFile(filePath)
        .then((data) => {
            let users = JSON.parse(data.content);
            users.push(req.body);
            fileHandler.writeFile(filePath, JSON.stringify(users, null, 2), () =>{
                }).then((data) => {
                    return res.end();
                }, (err) => {
                    res.statusCode = 404;
                    return res.end(JSON.stringify(err));
                });
        }, (err) => {
            res.statusCode = 404;
            return res.end(JSON.stringify(err));
        });
});

module.exports = router;

const getFileContents = (filePath) => {
  return new Promise( (resolve, reject) => {
    fs.readFile(filePath, 'utf8', (fileError, content) => {
      if(fileError) {
        return reject(fileError);
      }
      return JSON.parse(content);
    });
  });
};

const findRecordInFile = (filePath, options=[]) => {
  return new Promise( (resolve, reject) => {
    fs.readFile(filePath, 'utf8', (fileError, content) => {
      if(fileError) {
        return reject(fileError);
      }
      let returnObj;
      _.find(content, (obj, index) => {
        for(let key in options){
          if(options[key] == obj[key]){
            returnObj = {
              index: index,
              obj: obj};
            return true;
          }
        }
        return false;
      });
      return resolve(returnObj);
    });
  });
};
