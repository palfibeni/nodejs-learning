var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  let users = [{"name":"Benjámin","address":"Orosháza","age":"22"},{"name":"Zsolti","address":"Szeged","age":"28"}];
  res.render('index', { title: 'Express', users: users });
});

module.exports = router;
