

const users = [];

const handlePostRequests = (req, res) => {
    let content;
    let body = '';
    req.on('data', (chunk) => {
        body+=chunk;
    });
    if(req.url == '/newUser') {
        req.on('end', () => {
            addNewUser(res, body);
        });
    } else {
        content = `URL: ${req.url}, invalid request`;
        res.statusCode = 404;
        return res.end(content);
    }
};

module.exports = {handlePostRequests: handlePostRequests};

const addNewUser = (res, body) => {
    let user = JSON.parse(body);
    users.push(user);
    //console.log(users);
    content = "user saved";
    return res.end(content);
};
