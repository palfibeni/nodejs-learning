// load modules
const fs = require('fs');
const path = require('path');

const readFile = (filePath, callBack) => {
    let encoding = getEncoding(filePath);
    return new Promise( (resolve, reject) => {
        fs.readFile(filePath, encoding, (err, content) => {
            if(err){
                return reject(err);
            }
            return resolve({
                'content': content,
                'encoding': encoding
            });
        });
    });
};

module.exports = {readFile: readFile};

const getEncoding = (filePath) => {
    let fileExt = ['.jpg', '.jpeg', '.ttf', '.eot', '.png', '.woff', '.woff2', '.doc', '.docx'];
    let ext = path.extname(filePath).toLowerCase();
    return fileExt.indexOf(ext) > -1 ? 'binary' : 'utf8';
};
