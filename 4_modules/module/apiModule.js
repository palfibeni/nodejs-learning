//load modules
const path = require('path');
const fileHandler = require('./fileHandler');

const validPath = /^\/api\/([^\/]*)$/;
const validPathById = /^\/api\/([^\/]*)\/([^\/]*)$/;

const run = (req, res) => {
    if(validPath.test(req.url)) {
        let parts = req.url.match(validPath);
        let filePath = path.join('./json', parts[1] + '.json');
        fileHandler.readFile(filePath)
            .then((data) => {
                res.end(data.content, data.encoding);
            }, (err) => {
                res.statusCode = 404;
                return res.end(JSON.stringify(err));
            });
    }
};

module.exports = {
    run: run
};
