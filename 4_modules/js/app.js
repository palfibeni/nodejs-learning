var node = angular.module('node', []);

node.controller('BodyController', ['$scope', '$http', function($scope, $http){
    $http.get('/api/user')
        .then(function(serverResponse){
            console.log(serverResponse.data);
            $scope.users = serverResponse.data;
        }, function(err){
            console.log(err);
        });
}]);
