// load modules
var http = require('http');
const port = 9999;

const server = http.createServer( (req = {}, res = {}) => {
    runLog(req);
    getHandler(req, res);
});

server.listen(port);

const runLog = (req) => {
    let log = `URL: ${req.url}, Method: ${req.method} Time: ${new Date()}`;
    console.log(log);
};

const getHandler = (req, res) => {
    switch(req.method.toLowerCase()){
        case 'get':
            handleGetRequests(req, res);
            break;
        case 'post':
            handlePostRequests(req, res);
            break;
        default:
            return res.end('invalid request');
    }
};

const handleGetRequests = (req, res) => {
    let content;
    if(req.url == '/'){
        content = "index.html content";
        return res.end(content);
    } else {
        content = `FILE: ${req.url}`;
        res.statusCode = 404;
        return res.end(content);
    }
};

const users = [];
const handlePostRequests = (req, res) => {
    let content;
    let body = '';
    req.on('data', (chunk) => {
        body+=chunk;
    });
    if(req.url == '/newUser') {
        req.on('end', () => {
            addNewUser(res, body);
        });
    } else {
        content = `URL: ${req.url}, invalid request`;
        res.statusCode = 404;
        return res.end(content);
    }
};

const addNewUser = (res, body) => {
    let user = JSON.parse(body);
    users.push(user);
    console.log(users);
    content = "user saved";
    return res.end(content);
}

//Test:
// function p(){
//     var xhr = new XMLHttpRequest;
// 	xhr.open('post', '/newUser');
// 	xhr.onload = function(ev){
// 		console.log(ev.target.response)
//     };
// 	var user = {
//         name: 'Joe',
//         age: 21
//     };
// 	xhr.send(JSON.stringify(user));
// }
