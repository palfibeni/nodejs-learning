// load modules
var http = require('http');
const port = 9999;

const server = http.createServer( (req = {}, res = {}) => {
    let pali = new User('Pali');
    pali.greet();

    let data = JSON.stringify(order, null, 4);
    let content =
    `<pre>
        ${data}
    </pre>`;

    res.end(content);
});

server.listen(port);

const order = {
    __proto__: new Array,
    createDate: "2017-03-22 12:30:00",
    user: 44
};

order.push({
    name: 'Vasalo',
    price: 5261
});

// ES15 class:
class Person {

    constructor(name) {
        this.name = name;
    };

    greet() {
        console.log(`Hello, my name is ${this.name}!`);
    };
};

class User extends Person {

    constructor(name) {
        super(name);
        this.isLoggedIn = true;
    };
};



// old and ugly 'class' in js:
// const user = function(name){
//     this.construct = function(){
//         this.name = name;
//     };
//
//     greet = function(){
//         console.log(`Hello, my name is ${this.name}!`);
//     };
//
//     this.construct(name);
// };
