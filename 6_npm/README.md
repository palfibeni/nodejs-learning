# NodeJs learning 6. #

Node Package Manager (AKA Npm) is the NodeJs's heart and soul, every lib you will ever need will be available through this clever little guy.
It installs with NodeJs, no need for separate install.
You just run npm init in your project folder, and it creates a package.json file.
In your package.json file, will be the dependencies you add with npm install [dep] --save command, will be saved.
If you want to transfer your project somewhere else, or pass it to a buddy, this package.json will be needed only to load all the dependencies, and it's just the start.

details at https://www.npmjs.com/
