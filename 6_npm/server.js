// load modules
// nevesített moduloknál:
// 1. core modulok között keres,
// 2. lokális mappában van-e ilyen modul (node_modules)
// 3. appData vagy más os-en a default global mappa-ban keres,
// 4. error
const http = require('http');
const path = require('path');
const port = 9999;

const server = http.createServer( (req = {}, res = {}) => {
    return res.end();
});

server.listen(port);
