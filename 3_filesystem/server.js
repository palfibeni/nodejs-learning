// load modules
var http = require('http');
var fs = require('fs');
var path = require('path');
const port = 9999;

const server = http.createServer( (req = {}, res = {}) => {
    log(req);
    getHandler(req, res);
});

server.listen(port);

const log = (req) => {
    let log = `URL: ${req.url}, Method: ${req.method} Time: ${new Date()}`;
    console.log(log);
};

const getHandler = (req, res) => {
    switch(req.method.toLowerCase()){
        case 'get':
            handleGetRequests(req, res);
            break;
        case 'post':
            handlePostRequests(req, res);
            break;
        default:
            return res.end('invalid request');
    }
};

const handleGetRequests = (req, res) => {
    let content;
    let filePath = req.url == '/' ?
        './view/index.html' :
        path.join(__dirname, req.url);
    readFile(filePath)
        .then((data) => {
            res.end(data.content, data.encoding);
        }, (err) => {
            res.statusCode = 404;
            return res.end(JSON.stringify(err));
        });

    //example of Promise.all() data, and err is an array here
    // let p1 = readFile(filePath);
    // let p2 = readFile(filePath);
    // let p3 = readFile(filePath);
    // let p4 = readFile(filePath);
    // Promise.all([p1, p2, p3, p4]).then((data) => {
    //     res.end(data[0].content, data[0].encoding);
    // }, (err) => {
    //     res.statusCode = 404;
    //     return res.end(JSON.stringify(err[0]));
    // });
};

const readFile = (filePath, callBack) => {
    let encoding = getEncoding(filePath);
    return new Promise( (resolve, reject) => {
        fs.readFile(filePath, encoding, (err, content) => {
            if(err){
                return reject(err);
            }
            return resolve({
                'content': content,
                'encoding': encoding
            });
        });
    });
};

const getEncoding = (filePath) => {
    let fileExt = ['.jpg', '.jpeg', '.ttf', '.eot', '.png', '.woff', '.woff2', '.doc', '.docx'];
    let ext = path.extname(filePath).toLowerCase();
    return fileExt.indexOf(ext) > -1 ? 'binary' : 'utf8';
};

const users = [];
const handlePostRequests = (req, res) => {
    let content;
    let body = '';
    req.on('data', (chunk) => {
        body+=chunk;
    });
    if(req.url == '/newUser') {
        req.on('end', () => {
            addNewUser(res, body);
        });
    } else {
        content = `URL: ${req.url}, invalid request`;
        res.statusCode = 404;
        return res.end(content);
    }
};

const addNewUser = (res, body) => {
    let user = JSON.parse(body);
    users.push(user);
    //console.log(users);
    content = "user saved";
    return res.end(content);
};
