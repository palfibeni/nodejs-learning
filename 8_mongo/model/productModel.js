var mongoose = require('mongoose');

//előtte a mongod.exe futtatása
var Schema = mongoose.Schema;
var productSchema = new Schema({
  name: String,
  price: Number,
  quantity: Number
});

var User = mongoose.model('Product', productSchema);

module.exports = User;
