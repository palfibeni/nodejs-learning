var mongoose = require('mongoose');

//előtte a mongod.exe futtatása
var Schema = mongoose.Schema;
var userSchema = new Schema({
  name: String,
  email: {type: String, unique: true},
  address: String,
  age: Number,
  job: String
});

var User = mongoose.model('User', userSchema);

module.exports = User;
