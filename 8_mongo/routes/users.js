var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var User = require('../model/userModel');

/* GET users listing. */
router.get('/', function(req, res, next) {
  User.find((err, users) => {
    if(err){
      res.send(JSON.stringify(err));
    }
    res.json(users);
  });
});

router.get('/:id', function(req, res, next) {
    User.findOne({_id: req.params.id}, (err, users) => {
        if(err){
          res.send(JSON.stringify(err));
        }
        res.json(users);
    });
});

router.put('/', function(req, res, next) {
    new User(req.body)
    .save((err) => {
        if(err){
            res.send(JSON.stringify(err));
        }
        res.send('user saved');
        });
});

router.post('/:id', function(req, res, next) {
    User.update({_id: req.params.id}, {
            name: req.body.name,
            email: req.body.email,
            address: req.body.address,
            job: req.body.job,
        }, (err, raw) => {
        if(err){
            console.log(1);
            res.send(JSON.stringify(err));
        }
        res.json(raw);
    });
});

router.delete('/:id', function(req, res, next) {
    User.remove({_id: req.params.id}, (err) => {
        if(err){
          res.send(JSON.stringify(err));
        }
        res.json('success');
    });
});

module.exports = router;
