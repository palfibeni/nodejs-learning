var express = require('express');
var router = express.Router();
var path = require('path');
var config = require('../config');

/* GET users listing. */
router.get("/:fileName", function (req, res, next) {
  let filePath = path.join('templates', req.path);
  res.render(filePath);
});

module.exports = router;
