var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var Product = require('../model/productModel');

/* GET products listing. */
router.get('/', function(req, res, next) {
  Product.find((err, products) => {
    if(err){
      res.send(JSON.stringify(err));
    }
    res.json(products);
  });
});

router.get('/:id', function(req, res, next) {
    Product.findOne({_id: req.params.id}, (err, products) => {
        if(err){
          res.send(JSON.stringify(err));
        }
        res.json(products);
    });
});

router.put('/', function(req, res, next) {
    new Product(req.body)
    .save((err) => {
        if(err){
            res.send(JSON.stringify(err));
        }
        res.send('product saved');
        });
});

router.post('/:id', function(req, res, next) {
  Product.update({_id: req.params.id}, {
          name: req.body.name,
          price: req.body.price,
          quantity: req.body.quantity
      }, (err, raw) => {
    if(err){
      res.send(JSON.stringify(err));
    }
    res.json(raw);
  });
});

router.delete('/:id', function(req, res, next) {
    Product.remove({_id: req.params.id}, (err) => {
        if(err){
            console.log(err);
            res.send(JSON.stringify(err));
        }
        res.send('success');
    });
});

module.exports = router;
