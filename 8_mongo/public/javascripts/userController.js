node.controller('UserCtrl', ['$scope', '$http', '$state', function($scope, $http, $state){

    $scope.cols = ['name', 'email', 'address', 'job'];

    getData();

    $scope.updateUser = function(user){
        $state.go('newUser', ({
            action: 'edit',
            user: user
        }));
    };

    $scope.removeUser = function(user){
        $http.delete('/users/'+ user._id)
            .then(function(serverResponse){
                console.log(serverResponse.data);
                getData()
            }, function(err){
                console.log(err);
            });
    };

    function getData(){
        $http.get('/users')
            .then(function(serverResponse){
                console.log(serverResponse.data);
                $scope.users = serverResponse.data;
            }, function(err){
                console.log(err);
            });
    }
}]);
