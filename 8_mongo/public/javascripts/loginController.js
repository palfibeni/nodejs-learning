node.controller('LoginCtrl', ['$scope', '$http', '$state', function($scope, $http, $state){
    $scope.login = function(){
        console.log($scope.loginObj);
        $http.post('/login', JSON.stringify($scope.loginObj))
            .then((serverResponse) => {
                console.log(serverResponse.data);
                if(serverResponse.data.email){
                    $state.go('user');
                }

            }, (err) => {
                console.log(err);
            });
    }
}]);
