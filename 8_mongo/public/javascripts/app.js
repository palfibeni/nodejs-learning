var node = angular.module('node', ['ngRoute', 'ui.router'])

node.config(["$stateProvider", "$urlRouterProvider",
	function($stateProvider, $urlRouterProvider) {

	//$urlRouterProvider.otherwise('/');
	//view-k hozzáadása
	$stateProvider.state('main', {
		url: '/',
		templateUrl: "/partials/main"
	// }).state('login', {
	// 	url: '/login',
	// 	templateUrl: "/partials/login"
	}).state('user', {
		url: '/user',
		templateUrl: "/partials/user",
		controller:   "UserCtrl",
		controllerAs: "UserCtrl",
	}).state('newUser', {
		url: '/newUser',
		templateUrl: "/partials/newUser",
		controller:   "NewUserCtrl",
		controllerAs: "NewUserCtrl",
		params: {
			action: 'new',
			user: undefined
		}
	}).state('product', {
		url: '/product',
		templateUrl: "/partials/product",
		controller:   "ProductCtrl",
		controllerAs: "UserCtrl",
	}).state('newProduct', {
		url: '/newProduct',
		templateUrl: "/partials/newProduct",
		controller:   "NewProductCtrl",
		controllerAs: "NewProductCtrl",
		params: {
			action: 'new',
			product: undefined
		}
	});
}]);
