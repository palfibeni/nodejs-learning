node.controller('ProductCtrl', ['$scope', '$http', '$state', function($scope, $http, $state, $stateParams){

    $scope.cols = ['name', 'price', 'quantity'];

    getData();

    $scope.updateProduct = function(product){
        $state.go('newProduct', ({
            action: 'edit',
            product: product
        }));
    };

    $scope.removeProduct = function(product){
        $http.delete('/products/' + product._id)
            .then(function(serverResponse){
                console.log(serverResponse.data);
                getData();
            }, function(err){
                console.log(err);
            });
    };

    function getData(){
        $http.get('/products')
            .then(function(serverResponse){
                console.log(serverResponse.data);
                $scope.products = serverResponse.data;
            }, function(err){
                console.log(err);
            });
    }
}]);
