node.controller('NewUserCtrl', ['$scope', '$http', '$state', '$stateParams', function($scope, $http, $state, $stateParams){

    init();

    $scope.confirm = function(){
        if($stateParams.action == 'new'){
            addNewUser();
        } else {
            updateUser();
        }
    }

    function init(){
        console.log($stateParams.user);
        if($stateParams.action != 'new'){
            $scope.user = $stateParams.user;
        }
    }

    function addNewUser(){
        $http.put('/users/', JSON.stringify($scope.user))
            .then(function(serverResponse){
                console.log(serverResponse.data);
                $state.go('user');
            }, function(err){
                console.log(err);
            });
    }

    function updateUser(){
        $http.post('/users/' + $scope.user._id, JSON.stringify($scope.user))
            .then(function(serverResponse){
                console.log(serverResponse.data);
                $state.go('user');
            }, function(err){
                console.log(err);
            });
    }
}]);
