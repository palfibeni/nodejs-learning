node.controller('NewProductCtrl', ['$scope', '$http', '$state', '$stateParams', function($scope, $http, $state, $stateParams){

    init();

    $scope.confirm = function(){
        if($stateParams.action == 'new'){
            addNewProduct();
        } else {
            updateProduct();
        }
    }

    function init(){
        if($stateParams.action != 'new'){
            $scope.product = $stateParams.product;
        }
    }

    function addNewProduct(){
      console.log($scope.product);
        $http.put('/products/', JSON.stringify($scope.product))
            .then(function(serverResponse){
                console.log(serverResponse.data);
                $state.go('product');
            }, function(err){
                console.log(err);
            });
    };

    function updateProduct(product){
        $http.post('/products/' + $scope.product._id, JSON.stringify($scope.product))
            .then(function(serverResponse){
                console.log(serverResponse.data);
                $state.go('product');
            }, function(err){
                console.log(err);
            });
    };
}]);
