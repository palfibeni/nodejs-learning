var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../model/userModel');

const checkUser = (req, res, next) => {
    if(req.url == '/login' || req.url == '/login/check') {
            return next();
    }
    if(!req.cookies.mongo) {
        return res.redirect('/login');
    }
    User.find({token: req.cookies.mongo})
        .exec((err, user) => {
            if(err || !user) {
                console.log(err);
                return res.redirect('/login');
            }
            next();
        });
};

module.exports = checkUser;
