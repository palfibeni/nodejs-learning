# NodeJs learning 8. #

Big applications store datas in DB, that's why there is this final lesson, where we set up our persistence in a MongoDB.
MongoDB is not like Oracle's, or Microsoft's database, it is not limited to tables and columns.
Its queries are written in javascript, the record are stored in json, encoded for less memory use.

We use the mongoose object modelling tool, in this project, it can achive pretty awesome things.

### details at: ###
* mongoDB: https://www.mongodb.com/
* mongoose: http://mongoosejs.com/
