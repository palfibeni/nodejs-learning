// load modules
const path = require('path');
const fileHandler = require('./fileHandler');

const handleGetRequests = (req, res) => {
    let content;
    let filePath;

    switch(req.url){
        case '/':
            filePath = './view/index.html';
            break;
        case '/user':
            filePath = './view/user.html';
            break;
        case '/newUser':
            filePath = './view/newUser.html';
            break;
        default:
            filePath = path.join(__dirname, '..', req.url);
    }

    fileHandler.readFile(filePath)
        .then((data) => {
            res.end(data.content, data.encoding);
        }, (err) => {
            res.statusCode = 404;
            return res.end(JSON.stringify(err));
        });
};

module.exports = {handleGetRequests: handleGetRequests};

//example of Promise.all() data, and err is an array here
// const handleGetRequests = (req, res) => {
    //     let content;
    //     let filePath = req.url == '/' ?
    //         './view/index.html' :
    //         path.join(__dirname, '..', req.url);
    // let p1 = readFile(filePath);
    // let p2 = readFile(filePath);
    // let p3 = readFile(filePath);
    // let p4 = readFile(filePath);
    // Promise.all([p1, p2, p3, p4]).then((data) => {
    //     res.end(data[0].content, data[0].encoding);
    // }, (err) => {
    //     res.statusCode = 404;
    //     return res.end(JSON.stringify(err[0]));
    // });
// };
