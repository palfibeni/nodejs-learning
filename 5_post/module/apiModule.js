//load modules
const path = require('path');
const fileHandler = require('./fileHandler');

const validPath = /^\/api\/([^\/]*)$/;
const validPathNew = /^\/api\/([^\/]*)\/new$/;

const run = (req, res) => {
    if(validPath.test(req.url)) {
        findAll(req, res);
    }
    if(validPathNew.test(req.url)) {
        addNew(req, res);
    }
};

const findAll = (req, res) => {
    let parts = req.url.match(validPath);
    let filePath = path.join('./json', parts[1] + '.json');
    fileHandler.readFile(filePath)
        .then((data) => {
            res.end(data.content, data.encoding);
        }, (err) => {
            res.statusCode = 404;
            return res.end(JSON.stringify(err));
        });
};

const addNew = (req, res) => {
    let parts = req.url.match(validPathNew);
    let filePath = path.join('./json', parts[1] + '.json');
    let body = '';
    req.on('data', (chunk) => {
        body+=chunk;
    });
    req.on('end', () => {
        fileHandler.readFile(filePath)
        .then((data) => {
            let users = JSON.parse(data.content);
            users.push(JSON.parse(body));
            fileHandler.writeFile(filePath, JSON.stringify(users, null, 2), () =>{
            }).then((data) => {
                return res.end();
            }, (err) => {
                res.statusCode = 404;
                return res.end(JSON.stringify(err));
            });
        }, (err) => {
            res.statusCode = 404;
            return res.end(JSON.stringify(err));
        });
    });
};

module.exports = {
    run: run
};
