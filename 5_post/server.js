// load modules
const http = require('http');
const path = require('path');
const getHandler = require('./module/getHandler');
const postHandler = require('./module/postHandler');
const apiModule = require('./module/apiModule');
const port = 9999;

const server = http.createServer( (req = {}, res = {}) => {
    getRequestHandler(req, res);
});

server.listen(port);

const log = (req) => {
    let log = `URL: ${req.url}, Method: ${req.method} Time: ${new Date()}`;
    console.log(log);
};

const getRequestHandler = (req, res) => {
    //log(req);

    if( /^\/api\//.test(req.url) ){
        return apiModule.run(req, res);
    }

    switch(req.method.toLowerCase()){
        case 'get':
            getHandler.handleGetRequests(req, res);
            break;
        case 'post':
            postHandler.handlePostRequests(req, res);
            break;
        default:
            return res.end('invalid request');
    }
};
