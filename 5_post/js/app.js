var node = angular.module('node', ['ngRoute', 'ui.router'])

node.config(["$stateProvider", "$urlRouterProvider",
	function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/');
	// view-k hozzáadása
	$stateProvider.state('user', {
		url: '/',
		templateUrl: "view/user.html",
		controller:   "UserCtrl",
		controllerAs: "UserCtrl",
	}).state('newUser', {
		url: '/newUser',
		templateUrl: "view/newUser.html",
		controller:   "NewUserCtrl",
		controllerAs: "NewUserCtrl",
	});
}]);
