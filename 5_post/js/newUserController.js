node.controller('NewUserCtrl', ['$scope', '$http', '$state', function($scope, $http, $state){
    $scope.addNewUser = function(){
        console.log($scope.user);
        $http.post('/api/user/new', JSON.stringify($scope.user))
            .then(function(serverResponse){
                console.log(serverResponse.data);
                $state.go('user');
            }, function(err){
                console.log(err);
            });
    };
}]);
